//
//  DetailsViewController.swift
//  test.BigDigitals
//
//  Created by Artem on 6/8/17.
//  Copyright © 2017 Artem Velykyy. All rights reserved.
//

import UIKit
import CoreData

class DetailsViewController: UIViewController {

    var productArray = [product]()
    var indexPath: Int = 0
    
    //Outlets
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setData(index: indexPath)
        productDescription.sizeToFit()
        productDescription.adjustsFontSizeToFitWidth = true
        
    }
    
    func setData(index: Int) {
        let good = productArray[index]
        productImage.image = UIImage(data:good.productImage! as Data)
        let price = good.productPrice?.description as! String
        productPrice.text = "Price: \(price)"
        productDescription.text = good.productDescription
        productName.text = good.productName
        
    }

}
