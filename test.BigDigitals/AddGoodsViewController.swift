//
//  AddGoodsViewController.swift
//  test.BigDigitals
//
//  Created by Artem on 6/6/17.
//  Copyright © 2017 Artem Velykyy. All rights reserved.
//

import UIKit
import CoreData

class AddGoodsViewController: UIViewController, UIGestureRecognizerDelegate {

    
    private var goods: [NSManagedObject] = []
    
    //Outlets
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UITextField!
    @IBOutlet weak var productDescription: UITextView!
    @IBOutlet weak var productPrice: UITextField!
    
    //ACTIONS
    @IBAction func pickPhotoTapRecognizer(_ sender: UITapGestureRecognizer) {
        choosePhoto()
    }
    @IBAction func saveData(_ sender: Any) {
        let imageData = UIImagePNGRepresentation(productImage.image!) as NSData?
        let priceDouble = Double(productPrice.text!)
        CoreDataManager.storeObj(name: productName.text!, desc: productDescription.text!, price: priceDouble!, image: imageData!)
        setDefaultData()
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        productImage.isUserInteractionEnabled = true
    }
    
    func setDefaultData() {
        productImage.image = UIImage(named: "icons8-nui2")
        productName.text = ""
        productPrice.text = ""
        productDescription.text = ""
    }

}

extension AddGoodsViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func choosePhoto() {
        print("tap recognized")
        let camera = DSCameraHandler(delegate_: self)
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        optionMenu.popoverPresentationController?.sourceView = self.view
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert : UIAlertAction!) in
            camera.getCameraOn(self, canEdit: true)
        }
        let sharePhoto = UIAlertAction(title: "Photo Library", style: .default) { (alert : UIAlertAction!) in
            camera.getPhotoLibraryOn(self, canEdit: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert : UIAlertAction!) in
        }
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var image = info[UIImagePickerControllerEditedImage] as! UIImage
        image = image.resizeImage(newWidth: 100, newHeight: 100)
        
        productImage.image = image
        
        // image is our desired image
        
        picker.dismiss(animated: true, completion: nil)
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension UIImage {
    func resizeImage(newWidth: CGFloat, newHeight: CGFloat) -> UIImage {
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    } }
