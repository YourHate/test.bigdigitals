//
//  ViewController.swift
//  test.BigDigitals
//
//  Created by Artem on 6/6/17.
//  Copyright © 2017 Artem Velykyy. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UISearchBarDelegate {

    //Outlets
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var productArray = [product]()
    fileprivate var searchBar = UISearchBar()
    fileprivate let userDef = UserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTable()
        setUpSearchBar()
        self.hideKeyboardWhenTappedAround()
        
        if userDef.hasLaunchBefore == true {
        self.setDefaultData()
        }
        //CoreDataManager.cleanCoreData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        productArray = CoreDataManager.fetchObj()
        tableView.reloadData()
        searchBar.text = ""
    }
    
    
    //MARK: - search bar related
    fileprivate func setUpSearchBar() {
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 45))
        
//        searchBar.showsScopeBar = true
//        searchBar.scopeButtonTitles = ["name", "price", "desc"]
//        searchBar.selectedScopeButtonIndex = 0
        
        searchBar.delegate = self
        
        self.tableView.tableHeaderView = searchBar
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard !searchText.isEmpty else {
            productArray = CoreDataManager.fetchObj()
            tableView.reloadData()
            return
        }
        
        productArray = CoreDataManager.fetchObj(selectedScopeIdx: searchBar.selectedScopeButtonIndex, targetText:searchText)
        tableView.reloadData()
        print(searchText)
    }
    
    func setDefaultData() {
        CoreDataManager.storeObj(name: "iPhone 6", desc: "iPhone 6 32 Gb Silver", price: 200, image: (UIImagePNGRepresentation(UIImage(named: "iphone6")!) as NSData?)!)
        CoreDataManager.storeObj(name: "iPhone 5", desc: "iPhone 5 64 Gb Black", price: 150, image: (UIImagePNGRepresentation(UIImage(named: "iphone5")!) as NSData?)!)
        CoreDataManager.storeObj(name: "HTC 11", desc: "HTC 11 16 Gb Black", price: 100, image: (UIImagePNGRepresentation(UIImage(named: "htc")!) as NSData?)!)
        CoreDataManager.storeObj(name: "Samsung s8", desc: "Brand new smartphone!", price: 300, image: (UIImagePNGRepresentation(UIImage(named: "samsungS8")!) as NSData?)!)
    }
    
    func setTable() {
        tableView.rowHeight = 100.0
        tableView.register(UINib(nibName: "GoodsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }

}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("I was clicked")
        performSegue(withIdentifier: "toDetails", sender: indexPath.row)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetails" {
            if let indexPath = sender as? Int{
                let destVC = segue.destination as! DetailsViewController
                destVC.indexPath = indexPath
                destVC.productArray = productArray
            }
        }
    }

    
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {
            
            let good = productArray[indexPath.row]
            let cell =
                tableView.dequeueReusableCell(withIdentifier: "cell",
                                              for: indexPath) as! GoodsTableViewCell
            
            cell.productName.text = good.productName
            cell.productImageView.image = UIImage(data:good.productImage! as Data)
            let price = good.productPrice?.description as! String
            cell.productPrice.text = "Price: \(price)"
            
            return cell
    }
}

extension UserDefaults {
    
    var hasLaunchBefore: Bool {
        get {
            return self.bool(forKey: #function)
        }
        set {
            self.set(newValue, forKey: #function)
        }
    }
}

