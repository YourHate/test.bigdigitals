//
//  CoreDataManager.swift
//  test.BigDigitals
//
//  Created by Artem on 6/6/17.
//  Copyright © 2017 Artem Velykyy. All rights reserved.
//

import UIKit
import CoreData

struct product {
    
    var productName:String?
    var productDescription:String?
    var productPrice:Double?
    var productImage:NSData?
    
    init(name:String, desc:String, price:Double, image:NSData) {
        self.productName = name
        self.productDescription = desc
        self.productPrice = price
        self.productImage = image
    }
}

class CoreDataManager: NSObject {
    
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    ///store obj into core data
    class func storeObj(name:String, desc:String, price:Double, image:NSData) {
        let context = getContext()
        
        let entity = NSEntityDescription.entity(forEntityName: "Good", in: context)
        
        let managedObj = NSManagedObject(entity: entity!, insertInto: context)
        
        managedObj.setValue(name, forKey: "name")
        managedObj.setValue(desc, forKey: "desc")
        managedObj.setValue(price, forKey: "price")
        managedObj.setValue(image, forKey: "image")
        
        do {
            try context.save()
            print("saved!")
        } catch {
            print(error.localizedDescription)
        }
    }
    
    ///fetch all the objects from core data
    class func fetchObj(selectedScopeIdx:Int?=nil,targetText:String?=nil) -> [product]{
        var array = [product]()
        
        let fetchRequest:NSFetchRequest<Good> = Good.fetchRequest()
        
        if selectedScopeIdx != nil && targetText != nil{
            
            var filterKeyword = ""
            switch selectedScopeIdx! {
            case 0:
                filterKeyword = "name"
            case 1:
                filterKeyword = "desc"
            default:
                filterKeyword = "price"
            }

            var predicate = NSPredicate(format: "\(filterKeyword) contains[c] %@", targetText!)

        
            fetchRequest.predicate = predicate
        }
        
        do {
            let fetchResult = try getContext().fetch(fetchRequest)
            
            for item in fetchResult {
                let result = product(name:item.name!, desc:item.desc!, price:item.price, image:item.image!)
                array.append(result)
            }
        }catch {
            print(error.localizedDescription)
        }
        
        return array
    }

    ///delete all the data in core data
    class func cleanCoreData() {
        
        let fetchRequest:NSFetchRequest<Good> = Good.fetchRequest()
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
        
        do {
            print("deleting all contents")
            try getContext().execute(deleteRequest)
        }catch {
            print(error.localizedDescription)
        }
        
    }
    
}


