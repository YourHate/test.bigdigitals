//
//  GoodsTableViewCell.swift
//  test.BigDigitals
//
//  Created by Artem on 6/8/17.
//  Copyright © 2017 Artem Velykyy. All rights reserved.
//

import UIKit

class GoodsTableViewCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //print("Tap")
        // Configure the view for the selected state
    }

}
